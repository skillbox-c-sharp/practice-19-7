﻿using System.Collections;

namespace Challenge1.Models
{
    public class ContactRepository : IEnumerable<Contact>
    {
        List<Contact> contacts;

        public ContactRepository(List<Contact> contactList) 
        {
            contacts = contactList;
        }

        public void AddContact(Contact contact)
        {
            contacts.Add(contact);
        }

        public IEnumerator<Contact> GetEnumerator()
        {
            return contacts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Contact this[int id]
        {
            get { return contacts[id]; }
        }
    }
}

﻿using System.Xml.Linq;

namespace Challenge1.Models
{
    public class FileSaver
    {
        public static void SaveToFiles(ContactRepository clients)
        {
            List<Contact> contactList = clients.ToList();
            SaveToClientsFile(contactList);
        }

        private static void SaveToClientsFile(List<Contact> contactList)
        {
            XElement xContacts = new XElement("Contacts");
            for (int i = 0; i < contactList.Count; i++)
            {
                XElement contactElement = new XElement("Contact",
                        new XAttribute($"LastName", contactList[i].LastName),
                        new XAttribute($"FirstName", contactList[i].FirstName),
                        new XAttribute($"SurName", contactList[i].SurName),
                        new XElement("Id", contactList[i].Id),
                        new XElement("PhoneNumber", contactList[i].PhoneNumber),
                        new XElement("Address", contactList[i].Address),
                        new XElement("Description", contactList[i].Description));
                xContacts.Add(contactElement);
            }
            xContacts.Save("contacts.xml");
        }
    }
}

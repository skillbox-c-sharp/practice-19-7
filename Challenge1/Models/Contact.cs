﻿namespace Challenge1.Models
{
    public class Contact
    {
        public int Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string SurName { get; private set; }
        public string PhoneNumber { get; private set; }
        public string Address { get; private set; }
        public string Description { get; private set; }

        private static int currentId;

        static Contact()
        {
            currentId = 0;
        }

        public static int GetId()
        {
            return currentId;
        }

        public Contact(string firstName, string lastName, string surName,
                       string phoneNumber, string address, string descrtiption)
        {
            Id = currentId;
            FirstName = firstName;
            LastName = lastName;
            SurName = surName;
            PhoneNumber = phoneNumber;
            Address = address;
            Description = descrtiption;
            currentId++;
        }

        public Contact(int id, string firstName, string lastName, string surName,
                        string phoneNumber, string address, string descrtiption)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            SurName = surName;
            PhoneNumber = phoneNumber;
            Address = address;
            Description = descrtiption;
        }
    }
}

﻿using System.Globalization;
using System.Security.Principal;
using System.Xml.Linq;

namespace Challenge1.Models
{
    public class FileReader
    {

        public static ContactRepository ReadContactsFromFile()
        {
            CheckFilesExist();

            List<Contact> contacts = new List<Contact>();

            XDocument xdoc = XDocument.Load("contacts.xml");
            foreach (XElement element in xdoc.Element("Contacts").Elements("Contact"))
            {
                XAttribute lastNameAttribute = element.Attribute("LastName");
                XAttribute firstNameAttribute = element.Attribute("FirstName");
                XAttribute surNameAttribute = element.Attribute("SurName");
                XElement idElement = element.Element("Id");
                XElement phoneNumberElement = element.Element("PhoneNumber"); 
                XElement addressElement = element.Element("Address");
                XElement decriptionElement = element.Element("Description");

                int currentId = int.Parse(idElement.Value);
                contacts.Add(new Contact(currentId, lastNameAttribute.Value, firstNameAttribute.Value, surNameAttribute.Value,
                    phoneNumberElement.Value, addressElement.Value, decriptionElement.Value));

            }
            return ContactRepositoryFabric.GetContactRepository(contacts);
        }

        private static void CheckFilesExist()
        {
            if (!File.Exists("contacts.xml"))
            {
                File.Create("contacts.xml").Close();
                ContactRepository contacts = ContactRepositoryFabric.GetContactRepository(10);
                FileSaver.SaveToFiles(contacts);
            }
        }
    }
}

﻿using System.Collections.Generic;

namespace Challenge1.Models
{
    public static class ContactRepositoryFabric
    {
        public static ContactRepository GetContactRepository(int count)
        {
            List<Contact> list = new List<Contact>();
            for (int i = 0; i < count; i++)
            {
                Contact tempContact = new Contact($"Имя {i}", $"Фамилия {i}", $"Отчество {i}",
                    $"+790{i}56{i}5{i}{i}{i}", $"Адрес {i}", $"Описание {i}");
                list.Add(tempContact);
            }
            return new ContactRepository(list);
        }

        public static ContactRepository GetContactRepository(List<Contact> contacts)
        {
            return new ContactRepository(contacts);
        }
    }
}

﻿using Challenge1.Models;
using Microsoft.AspNetCore.Mvc;

namespace Challenge1.Controllers
{
    public class PhoneBookController : Controller
    {
        ContactRepository contacts;

        public PhoneBookController()
        {
            contacts = FileReader.ReadContactsFromFile();
        }

        public IActionResult Index()
        {
            return View(contacts);
        }

        public IActionResult ContactPage(int id)
        {
            return View(contacts[id]);
        }
    }
}
